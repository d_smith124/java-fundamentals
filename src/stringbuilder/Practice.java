package stringbuilder;

public class Practice {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder("");

		sb.append("my name is ");
		sb.append("");
		sb.append("david");

		System.out.println(sb.toString());

		StringBuilder s = new StringBuilder();
		s.append("my name is roger").append(" ").append("help meh");

		// formatting
		System.out.println("Here is some text. \t That was a tab. \nthat was a new line");
		// formatting integers
		System.out.printf("Total cost: %d Quantity: is %d\n", 5, 120);

		for (int i = 0; i < 5; i++) {
			System.out.printf("%2d: %s\n", i, "here is some text");
		}
		// formatting floating point values
		System.out.printf("Total value: %.2f\n", 5.614124124);
		System.out.printf("Total value: %-6.1f\n", 343.214124);
	}
}
