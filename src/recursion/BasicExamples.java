package recursion;

public class BasicExamples {
	public static void main(String[] args) {
		System.out.println(factorial(1));
		System.out.println(factorial(4));
		System.out.println(factorial(5));
		System.out.println(factorial(8));
		System.out.println(factorial(12));
		
		System.out.println();
		
		System.out.println(fibonacci(0));
		System.out.println(fibonacci(1));
		System.out.println(fibonacci(2));
		System.out.println(fibonacci(3));
		System.out.println(fibonacci(-1));
	}

	private static int factorial(int value) {
		if (value == 1) {
			return 1;
		}
		return factorial(value - 1) * value;
	}
	private static int fibonacci(int value) {
		if(value < 0) {
			return -1;
		} else if (value == 0) {
			return 0;
		} else if(value == 1) {
			return 1;
		} else {
			return fibonacci(value - 1) + fibonacci(value - 2);
		}
	}
}
