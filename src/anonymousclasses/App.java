package anonymousclasses;

class Machine {
	public void start() {
		System.out.println("starting machine");
	}
}

interface Plant {
	public void grow();
}

class App {
	public static void main(String[] args) {
		 Machine camera1 = new Machine() {
			 @Override public void start() {
				 System.out.println("Camera snapping");
			 }
		 };
		 
		 camera1.start();
		 
		 Plant plant1 = new Plant() {
			 public void grow() {
				 System.out.println("plant growing");
			 }
		 };
		 
		 plant1.grow();
	 }
}
