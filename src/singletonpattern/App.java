package singletonpattern;

public class App {
	public static void main(String[] args) {
		 
		// ---- Doesn't work b/c only Database constructor is private, 
		// ---- this makes sure only one instance of Database class can be created.
		// Database db = new Database();
		Database db = Database.getInstance();
		
		db.connect();
		db.disconnect();

	}
}
