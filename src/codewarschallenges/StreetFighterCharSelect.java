package codewarschallenges;

//https://www.codewars.com/kata/5853213063adbd1b9b0000be

public class StreetFighterCharSelect {
	public static void main(String[] args) {
		
	}

	public class Solution {
		public String[] streetFighterSelection(String[][] fighters, int[] position, String[] moves) {
			// ===========REFACTORED SOLUTION=============
			String[] fighterList = new String[moves.length];
			int top = position[0];
			int bottom = position[1];
			int i = 0;

			for (String move : moves) {
				switch (move) {
				case "up":
					// if at top stay at top, else move up
					top = (top == 0) ? ++top : --top;
					break;
				case "down":
					// if at top move down, else stay at bottom
					top = (top == 0) ? ++top : top;
					break;
				case "left":
					// if leftmost, set bottom to max length of that row, else stay leftmost
					bottom = (bottom == 0) ? fighters[top].length - 1 : --bottom;
					break;
				case "right":
					// if rightmost, move to leftmost, else move right 
					bottom = (bottom == fighters[top].length - 1) ? 0 : ++bottom;
					break;
				default:
					break;
				}
				fighterList[i++] = fighters[top][bottom];
			}
			return fighterList;
		}
	
		/*
		 * =========ORIGINAL SOLUTION============
		 * 
			String[] fighterList = new String[moves.length];
			int currentX = 0;
			int currentY = 0;
			for (int i = 0; i < moves.length; i++) {
				if (moves[i] == "up") {
					if (currentY - 1 >= 0) {
						currentY--;
						fighterList[i] = fighters[currentY][(currentX)];
					} else {
						fighterList[i] = fighters[currentY][(currentX)];
					}
				} else if (moves[i] == "down") {
					if (currentY + 1 < 2) {
						currentY++;
						fighterList[i] = fighters[currentY][(currentX)];
					} else {
						fighterList[i] = fighters[currentY][currentX];
					}
				} else if (moves[i] == "left") {
					if (currentX > 0) {
						currentX--;
						fighterList[i] = fighters[currentY][(currentX)];
					} else {
						currentX = 5;
						fighterList[i] = fighters[currentY][currentX];
					}
				} else if (moves[i] == "right") {
					if (currentX < 5) {
						currentX++;
						fighterList[i] = fighters[(currentY)][(currentX)];
					} else {
						currentX = 0;
						fighterList[i] = fighters[currentY][currentX];
					}
				}
			}
			return fighterList;
		}
		*/
	}
}
