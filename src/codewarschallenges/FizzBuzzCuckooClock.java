package codewarschallenges;

/*
 	https://www.codewars.com/kata/58485a43d750d23bad0000e6
 	When a minute is evenly divisible by three, the clock will say the word "Fizz".
	When a minute is evenly divisible by five, the clock will say the word "Buzz".
	When a minute is evenly divisible by both, the clock will say "Fizz Buzz", with two exceptions:
	On the hour, instead of "Fizz Buzz", the clock door will open, and the cuckoo bird will come out and "Cuckoo" between one and twelve times depending on the hour.
	On the half hour, instead of "Fizz Buzz", the clock door will open, and the cuckoo will come out and "Cuckoo" just once.
	With minutes that are not evenly divisible by either three or five, have the clock make a quiet, subtle "tick" sound.
	Your input will be a string containing hour and minute values in 24-hour time, separated by a colon, and with leading zeros.
	Your return value will be a string containing the combination of Fizz, Buzz, Cuckoo, and/or tick sounds that the clock needs to make at that time, separated by spaces. Note that although the input is in 24-hour time, cuckoo clocks' cuckoos are in 12-hour time.
*/
public class FizzBuzzCuckooClock {
	public static void main(String[] args) {
		System.out.println(fizzBuzzCuckooClock("13:34"));
		System.out.println(fizzBuzzCuckooClock("21:00"));
		System.out.println(fizzBuzzCuckooClock("11:15"));
		System.out.println(fizzBuzzCuckooClock("03:03"));
		System.out.println(fizzBuzzCuckooClock("14:30"));
		System.out.println(fizzBuzzCuckooClock("08:55"));
		System.out.println(fizzBuzzCuckooClock("00:00"));
		System.out.println(fizzBuzzCuckooClock("12:00"));
	}

	public static String fizzBuzzCuckooClock(String time) {
		int hour = Integer.parseInt(time.substring(0, 2));
		int minute = Integer.parseInt(time.substring(3));

		if (minute == 00) {
			String cuckoo = "";
			if (hour == 00) {
				return "Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo";
			} else if (hour > 12) {
				hour -= 12;
				for (int i = 0; i < hour - 1; i++) {
					cuckoo += "Cuckoo ";
				}
			} else {
				for (int i = 0; i < hour - 1; i++) {
					cuckoo += "Cuckoo ";
				}
			}
			cuckoo += "Cuckoo";
			return cuckoo;
		} else if (minute == 30) {
			return "Cuckoo";
		} else if (minute % 3 == 0 && minute % 5 == 0) {
			return "Fizz Buzz";
		} else if (minute % 3 == 0) {
			return "Fizz";
		} else if (minute % 5 == 0) {
			return "Buzz";
		} else {
			return "tick";
		}
	}
}

